package com.automationpractice;

import com.assertthat.selenium_shutterbug.core.Capture;
import com.assertthat.selenium_shutterbug.core.Shutterbug;
import com.automationpractice.enums.Title;
import com.automationpractice.pages.HomePage;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.*;
import org.testng.annotations.Optional;

import java.util.*;

public class ContactTest {

    // Class variables
    private static int counter = 0;
    private WebDriver driver;
    private HomePage homePage;

    @BeforeTest
    @Parameters("browser")
    public void setup (@Optional("chrome") String browser) {
        driver = DriverFactory.getDriver(browser);
    }

    @Test
    public void contactUsTest() {
        Logger LOG = LoggerFactory.getLogger(this.getClass());
        String projectUrl = System.getProperty("project.url");
        LOG.info("Navigate to {}", projectUrl);
        driver.get(projectUrl);
        homePage = new HomePage(driver);
        homePage.goToContact()
                .chooseSubjectHeading("2")
                .fillEmailAddress("example@exaple.com")
                .fillOrderReference("A63GDAHD36")
                .uploadImage()
                .fillMessage("Thank you for the fast shipment!")
                .sendTicket();
    }

    @AfterMethod
    public void tearDown(ITestResult result) {
        if(!result.isSuccess()) {
            Shutterbug.shootPage(driver, Capture.FULL, 500, true )
                    .withName("screenshot_"+(counter++)).save();
        }
    }

    @AfterTest
    public void cleanUp() { driver.quit(); }

}
