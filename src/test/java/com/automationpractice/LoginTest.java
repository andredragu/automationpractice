package com.automationpractice;

import com.assertthat.selenium_shutterbug.core.Capture;
import com.assertthat.selenium_shutterbug.core.Shutterbug;
import com.automationpractice.pages.HomePage;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.time.Duration;

public class LoginTest {

    // Class variables
    private static int counter = 0;
    private WebDriver driver;
    private HomePage homePage;


    @BeforeTest
    @Parameters("browser")
    public void setup (@Optional("chrome") String browser) {
        driver = DriverFactory.getDriver(browser);
    }

    @Test
    public void loginTest() {
        Logger LOG = LoggerFactory.getLogger(this.getClass());
        String projectUrl = System.getProperty("project.url");
        LOG.info("Navigate to {}", projectUrl);
        driver.get(projectUrl);
        homePage = new HomePage(driver);
        homePage.goToSignIn().logIn("moscraciuncupletedalbe@hoho.com","MyPa$$w0rdIsStronk");
    }

    @AfterMethod
    public void tearDown(ITestResult result){
        if(!result.isSuccess()){
            Shutterbug.shootPage(driver, Capture.FULL,500,true)
                    .withName("screenshot_"+(counter++)).save();
        }
    }

    @AfterTest
    public void cleanUp(){
        driver.quit();
    }
}
