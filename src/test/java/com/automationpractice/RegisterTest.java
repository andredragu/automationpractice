package com.automationpractice;

import com.assertthat.selenium_shutterbug.core.Capture;
import com.assertthat.selenium_shutterbug.core.Shutterbug;
import com.automationpractice.enums.Title;
import com.automationpractice.pages.HomePage;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.util.Random;

public class RegisterTest {

    // Class variables
    private static int counter = 0;
    private WebDriver driver;
    private HomePage homePage;

    @BeforeTest
    @Parameters("browser")
    public void setup (@Optional("chrome") String browser) {
        driver = DriverFactory.getDriver(browser);
    }

    @Test
    public void registerTest() {
        Logger LOG = LoggerFactory.getLogger(this.getClass());
        String projectUrl = System.getProperty("project.url");
        LOG.info("Navigate to {}", projectUrl);
        driver.get(projectUrl);
        homePage = new HomePage(driver);
        Random rand = new Random();

        homePage.goToSignIn()
                .register("moscraciuncupletedalbe@hoho.com")
                .checkTitle(Title.MR)
                .fillPassword("MyPa$$w0rdIsStronk")
                .fillName("TestFName","TestLName")
                .fillBirthDate("29","1","1980")
                .fillAddress("TestFName","TestLName","Some address","Bear","12345","Delaware","12345678")
                .clickSignUp();
    }

    @AfterMethod
    public void tearDown(ITestResult result){
        if(!result.isSuccess()){
            Shutterbug.shootPage(driver, Capture.FULL,500,true)
                    .withName("screenshot_"+(counter++)).save();
        }
    }

    @AfterTest
    public void cleanUp(){
        driver.quit();
    }
}
