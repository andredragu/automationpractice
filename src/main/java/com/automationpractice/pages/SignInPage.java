package com.automationpractice.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SignInPage extends BasePage{

    // Class variables
    private final By email = By.id("email");                    // User email Field
    private final By email_create = By.id("email_create");      // New email Field
    private final By password = By.id("passwd");                // Password Field
    private final By loginButton = By.id("SubmitLogin");        // Login Button
    private final By signupButton = By.id("SubmitCreate");      // Signup Button

    // Constructor for SignInPage class
    public SignInPage(WebDriver driver) {
        super(driver);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        wait.until(ExpectedConditions.presenceOfElementLocated(email_create));
    }

    // logIn() - logs the user in
    public UserPage logIn(String user, String passwd){
        driver.findElement(email).sendKeys(user);
        driver.findElement(password).sendKeys(passwd);
        driver.findElement(loginButton).click();
        return new UserPage(driver);
    }

    // register() - registers a new user
    public RegisterPage register(String user){
        driver.findElement(email_create).sendKeys(user);
        driver.findElement(signupButton).click();
        return new RegisterPage(driver);
    }
}
