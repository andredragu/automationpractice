package com.automationpractice.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ContactUsPage extends BasePage {

    // Class variables
    private final By subjectHeading = By.id("id_contact");          // Subject Dropdown
    private final By emailAddress = By.id("//*[@id=\"email\"]");    // Email Field
    private final By orderReference = By.id("id_order");            // Order reference Field
    private final By chooseFile = By.id("fileUpload");           // Choose file Button
    private final By messageArea = By.id("message");                // Message area Field
    private final By sendButton = By.id("submitMessage");           // Send Button

    // Constructor for ContactUsPage class
    public ContactUsPage(WebDriver driver) {
        super(driver);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        wait.until(ExpectedConditions.presenceOfElementLocated(emailAddress));
    }

    // chooseSubjectheading() - chooses a heading for the ticket
    public ContactUsPage chooseSubjectHeading(String option) {
        Select subjectSelect = new Select(driver.findElement(subjectHeading));
        subjectSelect.selectByValue(option);
        return this;
    }

    // fillEmailAddress() - fills in the user's email address
    public ContactUsPage fillEmailAddress(String email) {
        driver.findElement(emailAddress).sendKeys(email);
        return this;
    }

    // fillOrderReference() - fills in the order reference
    public ContactUsPage fillOrderReference(String reference) {
        driver.findElement(orderReference).sendKeys(reference);
        return this;
    }

    // uploadImage() - uploads an image
    public ContactUsPage uploadImage() {
        driver.findElement(chooseFile).sendKeys("C:\\Users\\ADragu\\test.png");
        return this;
    }

    // fillMessage() - fills in the message field
    public ContactUsPage fillMessage(String message) {
        driver.findElement(messageArea).sendKeys(message);
        return this;
    }

    // sendTicket() - clicks the send button
    public ContactUsPage sendTicket() {
        driver.findElement(sendButton).click();
        return this;
    }
}
