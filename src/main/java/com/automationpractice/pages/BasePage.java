package com.automationpractice.pages;

import org.openqa.selenium.WebDriver;

public class BasePage {

    // Initialize web Driver
    protected WebDriver driver;

    // Constructor for the BasePage class
    public BasePage(WebDriver driver) {
        this.driver = driver;
    }
}
