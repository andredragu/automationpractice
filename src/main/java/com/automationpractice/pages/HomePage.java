package com.automationpractice.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    // Class variable
    private final By signInLink = By.cssSelector("a.login");
    private final By contactUsLink = By.xpath("//*[@id=\"contact-link\"]/a");
    private final By shoppingCart = By.id("shopping_cart");

    // Constructor for HomePage class
    public HomePage(WebDriver driver) {
        super(driver);
    }

    // goToSignIn() - the Sign In button from the Home Page
    public SignInPage goToSignIn() {
        driver.findElement(signInLink).click();
        return new SignInPage(driver);
    }

    // goToContact() - the Contact Us button from the Home Page
    public ContactUsPage goToContact() {
        driver.findElement(contactUsLink).click();
        return new ContactUsPage(driver);
    }

    // goToShoppingCart() - the Cart button from the HomePage
    public CartPage goToShoppingCart() {
        driver.findElement(shoppingCart).click();
        return new CartPage(driver);
    }
}
