package com.automationpractice.pages;

import com.automationpractice.enums.Title;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class RegisterPage extends BasePage {

    // Class variables
    private final By mrRadioButton = By.id("id_gender1");               // Mr RadioButton
    private final By mrsRadioButton = By.id("id_gender2");              // Mrs RadioButton
    private final By custFirstName = By.id("customer_firstname");       // Customer first name Field
    private final By custLastName = By.id("customer_lastname");         // Customer last name Field
    private final By passwd = By.id("passwd");                          // Password Field
    private final By birthDay = By.id("days");                          // Days Dropdown
    private final By birthMonth = By.id("months");                      // Months Dropdown
    private final By birthYear = By.id("years");                        // Years Dropdown
    private final By newsletterCheckbox = By.id("newsletter");          // Newsletter Checkbox
    private final By offersCheckbox = By.id("optin");                   // Offers Checkbox
    private final By addressFirstName = By.id("firstname");             // Address first name Field
    private final By addressLastName = By.id("lastname");               // Address last name Field
    private final By addressAddress1 = By.id("address1");               // Address 1 Field
    private final By addressAddress2 = By.id("address2");               // Address 2 Field
    private final By addressCity = By.id("city");                       // City Field
    private final By addressZipCode = By.id("postcode");                // Address zipcode Field
    private final By addressState = By.id("id_state");                  // State Field
    private final By addressPhone = By.id("phone_mobile");              // Mobile phone Field
    private final By signUpButton = By.id("submitAccount");             // Submit account Button

    // Constructor for RegisterPage class
    public RegisterPage(WebDriver driver) {
        super(driver);
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(20));
        wait.until(ExpectedConditions.presenceOfElementLocated(mrRadioButton));
    }

    // clickSignUp() - clicks the Sign Up button
    public UserPage clickSignUp() {
        driver.findElement(signUpButton).click();
        return new UserPage(driver);
    }

    // checkTitle() - checks the corresponding title (Mr, Mrs)
    public RegisterPage checkTitle(Title title) {
        switch (title) {
            case MR:
                driver.findElement(mrRadioButton).click();
                break;
            case MRS:
                driver.findElement(mrsRadioButton).click();
                break;
        }
        return this;
    }

    // fillName() - fills in the name of the user
    public RegisterPage fillName(String firstName, String lastName) {
        driver.findElement(custFirstName).sendKeys(firstName);
        driver.findElement(custLastName).sendKeys(lastName);
        return this;
    }

    // fillBirthDate() - fills in the birth date of the user
    public RegisterPage fillBirthDate(String day, String month, String year) {
        Select daySel = new Select(driver.findElement(birthDay));
        Select monthSel = new Select(driver.findElement(birthMonth));
        Select yearSel = new Select(driver.findElement(birthYear));
        daySel.selectByValue(day);
        monthSel.selectByValue(month);
        yearSel.selectByValue(year);
        return this;
    }

    // fillPassword() - fills in the password
    public RegisterPage fillPassword(String password) {
        driver.findElement(passwd).sendKeys(password);
        return this;
    }

    // checkNewsletter() - checks the newsletter checkbox
    public RegisterPage checkNewsletter() {
        driver.findElement(newsletterCheckbox).click();
        return this;
    }

    // checkOffers() - checks the offers checkbox
    public RegisterPage checkOffers() {
        driver.findElement(offersCheckbox).click();
        return this;
    }

    // fillAddress() - fills in the complete address
    public RegisterPage fillAddress(String firstName, String lastName, String address, String city,
                                    String zipcode, String state, String phone) {
        driver.findElement(addressFirstName).sendKeys(firstName);
        driver.findElement(addressLastName).sendKeys(lastName);
        driver.findElement(addressAddress1).sendKeys(address);
        driver.findElement(addressCity).sendKeys(city);
        driver.findElement(addressPhone).sendKeys(phone);
        driver.findElement(addressZipCode).sendKeys(zipcode);
        Select select = new Select(driver.findElement(addressState));
        select.selectByVisibleText(state);
        return this;
    }

}
